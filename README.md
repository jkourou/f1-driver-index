![F1 logo](./src/assets/img/logo_small.png)

Driver Index
======================

This is a simple SPA build with Angular.

## Notes
- [Pure](httpshttps://purecss.io/) is used for normalization and responsive grid. No other library was used for presentation purposes.
- Build with responsive principles, but not actually responsive.

## Next Versions
- A single `Grid` (dumb) component should be introduced and used to render all grids in the app.
- A single `Loader` (dumb) component for all loading animations while waiting to fetch and populate data.
- A single `NoData` (dumb) component when no data are fetched after API call or filter.
- HTTP error handling should be more intuitive, with messages printed clearly to the user, and interceptors to handle expected cases and exceptions.
- When navigating from the Driver List to the Driver Profile, driver basic data should not be fetched again from the API, but instead should be passed from the Driver List to the Driver Profile.
- Grid filtering is done used a single input field, for all columns. A more intuitive filtering system should also be implemented, per column, using other form elements (e.g. dropdowns), where appropriate.
- Current filtering should implement a debounce method to avoid firing in every keystroke.
- Pagination should be smarter, showing number of pages, current page etc. Also the pagination method should navigate to the appropriate page, and this by itself should fire the getDrivers() method.
- Catch all and 404 routes and page

## Unit Tests
Basic unit Tests have been implemented, i.e. creation checks with a few minor exceptions, like in the `driver-list-component`.

## API used
[https://ergast.com/mrd/](https://ergast.com/mrd/)

## Examples/guides used
Basic App Skeleton
[https://angular.io/tutorial/toh-pt1](https://angular.io/tutorial/toh-pt1)

Sorting
[https://www.carbonatethis.com/sort-table-columns-with-angular-and-typescript/](https://www.carbonatethis.com/sort-table-columns-with-angular-and-typescript/)

## Disclaimers
- This app is tested in Firefox and Chrome desktop versions
- This is my first Angular2+ app, and first usage of TypeScript.

---

Copyright (c) 2019 jKourou