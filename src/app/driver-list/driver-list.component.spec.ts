import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick
}                                           from '@angular/core/testing';
import { FormsModule }                      from '@angular/forms'
import { RouterTestingModule }              from '@angular/router/testing';
import { HttpClientTestingModule }          from '@angular/common/http/testing';
import { By }                               from '@angular/platform-browser';
import { DriverListComponent }              from './driver-list.component';


describe('DriverListComponent', () => {
  let component: DriverListComponent;
  let fixture: ComponentFixture<DriverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes([
          {path: 'drivers/:offset', component: DriverListComponent}
        ]),
        HttpClientTestingModule
      ],
      declarations: [ DriverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should paginate', fakeAsync(() => {
    component.ngOnInit();
    tick();
    spyOn(component, 'pagination');
    let button = fixture.debugElement.query(By.css('.pagination > button:last-child'));
    button.triggerEventHandler('click', null);
    tick();
    expect(component.pagination).toHaveBeenCalled();
}));
});
