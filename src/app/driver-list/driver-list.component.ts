import { Component, OnInit }				from '@angular/core';
import { Router, ActivatedRoute, ParamMap }	from '@angular/router';
import { Driver }							from '../driver';
import { DriverService }					from '../driver.service';

@Component({
	selector: 'app-driver-list',
	templateUrl: './driver-list.component.html',
	styleUrls: ['./driver-list.component.scss']
})
export class DriverListComponent implements OnInit {
	drivers: Driver[];
	driversFiltered: Driver[];
	latestSort: object;
	filterValue = '';
	driversLoading = false;
	offset: number;
	allowFwd: boolean;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private driverService: DriverService
	) {
		this.drivers = [];
		this.driversFiltered = [];
		this.latestSort = {sortColumn: '', sortDirection: ''};
		this.offset = 0;
		this.allowFwd = true;
	}

	ngOnInit() {
		this.offset = Number(this.route.snapshot.paramMap.get('offset'));
		this.getDrivers(this.offset);
	}

	getDrivers(offset: number): void {
		this.driversLoading = true;
		this.driversFiltered = [];
		this.driverService.getDrivers(offset)
			.subscribe(resp => {
				this.driversFiltered = this.drivers = resp.drivers;
				this.allowFwd = Number(this.offset) + Number(resp.limit) < resp.total;
				this.filterGrid(this.filterValue);
				this.driversLoading = false;
			});
	}

	pagination(direction: string) {
		if (direction === 'fwd')
			this.offset += 30;
		else if (direction === 'bwd')
			this.offset -= 30;

		this.router.navigate(['/drivers', this.offset]);
		this.getDrivers(this.offset);
	}

	filterGrid($event: string) {
		let filteredResults = [];
		filteredResults = this.drivers.filter(function(row) {
			return Object.keys(row).some(function(key) {
				return String(row[key]).toLowerCase().indexOf($event) > -1;
			})
		})

		this.driversFiltered = filteredResults;
		this.sortGrid(this.latestSort);
	}

	sortGrid($event) {
		this.latestSort = $event;
		if (this.driversFiltered)
			return this.driversFiltered.sort((a,b) => {
				if ($event.sortDirection === 'desc')
					return a[$event.sortColumn] < b[$event.sortColumn]
							? 1
							: -1;
				else
					return a[$event.sortColumn] > b[$event.sortColumn]
							? 1
							: -1;

			});
	}
}
