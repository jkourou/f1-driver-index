import { Component, OnInit } 	from '@angular/core';
import { ActivatedRoute } 		from '@angular/router';
import { Location } 			from '@angular/common';
import { Driver }				from '../driver';
import { DriverService }		from '../driver.service';

@Component({
	selector: 'app-driver-profile',
	templateUrl: './driver-profile.component.html',
	styleUrls: ['./driver-profile.component.scss']
})
export class DriverProfileComponent implements OnInit {
	driverInfo: Driver = {
		driverId	: '',
		url			: '',
		givenName	: '',
		familyName	: '',
		dateOfBirth	: '',
		nationality	: ''
	};
	driverRaceResults = [];
	driverRaceResultsFiltered = [];
	latestSort: object;
	raceResultsLoading = false;

	constructor(
		private route: ActivatedRoute,
		private location: Location,
		private driverService: DriverService
	) {
		this.latestSort = {sortColumn: '', sortDirection: ''};
	}

	ngOnInit() {
		const driverId: string = this.route.snapshot.paramMap.get('id') || '';
		this.getDriverInfo(driverId);
		this.getDriverRaceResults(driverId);
	}

	getDriverInfo(driverId: string): void {
		this.driverService.getDriverInfo(driverId)
			.subscribe((driverInfo: Driver) => this.driverInfo = driverInfo);
	}

	getDriverRaceResults(driverId: string): void {
		this.raceResultsLoading = true;
		this.driverService.getDriverRaceResults(driverId)
			.subscribe(driverRaceResults => {
				this.raceResultsLoading = false;
				this.driverRaceResultsFiltered = this.driverRaceResults = driverRaceResults
			});
	}

	filterGrid($event: string) {
		let filteredResults = [];
		filteredResults = this.driverRaceResults.filter(function(row) {
			return Object.keys(row).some(function(key) {
				return String(row[key]).toLowerCase().indexOf($event) > -1;
			})
		})

		this.driverRaceResultsFiltered = filteredResults;
		this.sortGrid(this.latestSort);
	}

	sortGrid($event) {
		this.latestSort = $event;
		if (this.driverRaceResultsFiltered)
			return this.driverRaceResultsFiltered.sort((a,b) => {
				let sortWhat = 'string';
				if (!isNaN(a[$event.sortColumn]))
					sortWhat = 'number';

				if ($event.sortDirection === 'desc') {
					if (sortWhat === 'number')
						return b[$event.sortColumn] - a[$event.sortColumn]

						return a[$event.sortColumn] < b[$event.sortColumn]
							? 1
							: -1;
				} else {
					if (sortWhat === 'number')
						return a[$event.sortColumn] - b[$event.sortColumn]

						return a[$event.sortColumn] > b[$event.sortColumn]
							? 1
							: -1;
				}
			});
	}

	goBack(): void {
		this.location.back();
	}
}
