import { Injectable }				from '@angular/core';
import { HttpClient, HttpHeaders } 	from '@angular/common/http';
import { Observable, of } 			from 'rxjs';
import { map, tap, catchError } 	from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class DriverService {
	constructor(
		private http: HttpClient
  	) { }

	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(error); // log to console instead

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	getDrivers(offset: number): Observable<any> {
		return this.http.get<any>(`http://ergast.com/api/f1/drivers.json?offset=${offset}`)
			.pipe(
				map(drivers => {
					return {
						drivers: drivers.MRData.DriverTable.Drivers,
						limit: drivers.MRData.limit,
						total: drivers.MRData.total
					}
				}),
				catchError(this.handleError<any>('getDrivers', []))
			);
	}

	getDriverInfo(driverId: string): Observable<any> {
		return this.http.get<any>(`http://ergast.com/api/f1/drivers/${driverId}.json`)
			.pipe(
				map(driverInfo => driverInfo.MRData.DriverTable.Drivers[0]),
				catchError(this.handleError<any>('getDriverInfo', []))
			);
	}

	getDriverRaceResults(driverId: string): Observable<any> {
		return this.http.get<any>(`http://ergast.com/api/f1/drivers/${driverId}/results.json`)
			.pipe(
				map(driverRaceResultsRaw => {
					let driverRaceResults = [];
					driverRaceResults = driverRaceResultsRaw.MRData.RaceTable.Races.map(result => {
						return {
							season		: result.season,
							round		: result.round,
							raceName	: result.raceName,
							position	: result.Results[0].position,
							points		: result.Results[0].points,
							grid		: result.Results[0].grid,
							laps		: result.Results[0].laps,
							constructor : result.Results[0].Constructor.name
						}
					});
					return driverRaceResults
				}),
				catchError(this.handleError<any>('getDriverRaceResults', []))
			);
	}
}