import { async, TestBed } 			from '@angular/core/testing';
import { HttpClientTestingModule }  from '@angular/common/http/testing';

import { DriverService } from './driver.service';

describe('DriverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  it('should be created', () => {
    const service: DriverService = TestBed.get(DriverService);
    expect(service).toBeTruthy();
  });
});
