import { NgModule } 				from '@angular/core';
import { Routes, RouterModule } 	from '@angular/router';

import { DriverListComponent }  	from './driver-list/driver-list.component';
import { DriverProfileComponent }  	from './driver-profile/driver-profile.component';

const routes: Routes = [
	{
		path: 'drivers/:offset',
		component: DriverListComponent
	},
	{
		path: 'driver/:id',
		component: DriverProfileComponent
	},
	{
		path: '',
		redirectTo: 'drivers/0',
		pathMatch: 'full'
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
