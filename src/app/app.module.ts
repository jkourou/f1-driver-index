import { BrowserModule }			from '@angular/platform-browser';
import { NgModule }					from '@angular/core';
import { HttpClientModule }			from '@angular/common/http';
import { FormsModule }				from '@angular/forms'
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppRoutingModule }			from './app-routing.module';
import { AppComponent }				from './app.component';
import { DriverProfileComponent }	from './driver-profile/driver-profile.component';
import { DriverListComponent }		from './driver-list/driver-list.component';
import { SortableColumnComponent }	from './sortable-column/sortable-column.component';
import { SortableTableDirective }	from './sortable-table.directive';

@NgModule({
	declarations: [
		AppComponent,
		DriverProfileComponent,
		DriverListComponent,
		SortableColumnComponent,
		SortableTableDirective
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		AngularFontAwesomeModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
